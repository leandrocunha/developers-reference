.. _l10n:

Internationalization and Translations
********************************************************************************************************************************

Debian supports an ever-increasing number of natural languages. Even if
you are a native English speaker and do not speak any other language, it
is part of your duty as a maintainer to be aware of issues of
internationalization (abbreviated i18n because there are 18 letters
between the 'i' and the 'n' in internationalization). Therefore, even if
you are ok with English-only programs, you should read most of this
chapter.

According to `Introduction to
i18n <https://www.debian.org/doc/manuals/intro-i18n/>`__ from Tomohiro
KUBOTA, I18N (internationalization) means modification of software or
related technologies so that software can potentially handle multiple
languages, customs, and other differences, while L10N (localization)
means implementation of a specific language for
already-internationalized software.

l10n and i18n are interconnected, but the difficulties related to each
of them are very different. It's not really difficult to allow a program
to change the language in which texts are displayed based on user
settings, but it is very time consuming to actually translate these
messages. On the other hand, setting the character encoding is trivial,
but adapting the code to use several character encodings is a really
hard problem.

Setting aside the i18n problems, where no general guideline can be
given, there is actually no central infrastructure for l10n within
Debian which could be compared to the buildd mechanism for porting. So
most of the work has to be done manually.

.. _l10n-handling:

How translations are handled within Debian
================================================================================================================================

Handling translation of the texts contained in a package is still a
manual task, and the process depends on the kind of text you want to see
translated.

For program messages, the gettext infrastructure is used most of the
time. Most of the time, the translation is handled upstream within
projects like the `Free Translation
Project <https://translationproject.org/html/welcome.html>`__, the
`GNOME Translation
Project <https://wiki.gnome.org/TranslationProject>`__ or the `KDE
Localization <https://l10n.kde.org/>`__ project. The only centralized
resources within Debian are the `Central Debian translation
statistics <https://www.debian.org/intl/l10n/>`__, where you can find
some statistics about the translation files found in the actual
packages, but no real infrastructure to ease the translation process.

Package descriptions have translations since many years and Maintainers
don't need to do anything special to support translated package
descriptions; translators should use the `Debian Description Translation
Project (DDTP) <https://ddtp.debian.org/>`__.

For ``debconf`` templates, maintainers should use the ``po-debconf``
package to ease the work of translators, who could use the DDTP to do
their work (but the French and Brazilian teams don't). Some statistics
can be found both on the `DDTP site <https://ddtp.debian.org/>`__
(about what is actually translated), and on the `Central Debian
translation statistics <https://www.debian.org/intl/l10n/>`__ site
(about what is integrated in the packages).

For web pages, each l10n team has access to the relevant VCS, and the
statistics are available from the Central Debian translation statistics
site.

For general documentation about Debian, the process is more or less the
same as for the web pages (the translators have access to the VCS), but
there are no statistics pages.

For package-specific documentation (man pages, info documents, other
formats), almost everything remains to be done.

.. _l10n-faqm:

I18N & L10N FAQ for maintainers
================================================================================================================================

This is a list of problems that maintainers may face concerning i18n and
l10n. While reading this, keep in mind that there is no real consensus
on these points within Debian, and that this is only advice. If you have
a better idea for a given problem, or if you disagree on some points,
feel free to provide your feedback, so that this document can be
enhanced.

.. _l10n-faqm-tr:

How to get a given text translated
--------------------------------------------------------------------------------------------------------------------------------

To translate package descriptions or ``debconf`` templates, you have
nothing to do; the DDTP infrastructure will dispatch the material to
translate to volunteers with no need for interaction on your part.

For all other material (gettext files, man pages, or other
documentation), the best solution is to put your text somewhere on the
Internet, and ask on debian-i18n for a translation in different
languages. Some translation team members are subscribed to this list,
and they will take care of the translation and of the reviewing process.
Once they are done, you will get your translated document from them in
your mailbox.

.. _l10n-faqm-rev:

How to get a given translation reviewed
--------------------------------------------------------------------------------------------------------------------------------

From time to time, individuals translate some texts in your package and
will ask you for inclusion of the translation in the package. This can
become problematic if you are not fluent in the given language. It is a
good idea to send the document to the corresponding l10n mailing list,
asking for a review. Once it has been done, you should feel more
confident in the quality of the translation, and feel safe to include it
in your package.

.. _l10n-faqm-update:

How to get a given translation updated
--------------------------------------------------------------------------------------------------------------------------------

If you have some translations of a given text lying around, each time
you update the original, you should ask the previous translator to
update the translation with your new changes. Keep in mind that this
task takes time; at least one week to get the update reviewed and all.

If the translator is unresponsive, you may ask for help on the
corresponding l10n mailing list. If everything fails, don't forget to
put a warning in the translated document, stating that the translation
is somehow outdated, and that the reader should refer to the original
document if possible.

Avoid removing a translation completely because it is outdated. Old
documentation is often better than no documentation at all for
non-English speakers.

.. _l10n-faqm-bug:

How to handle a bug report concerning a translation
--------------------------------------------------------------------------------------------------------------------------------

The best solution may be to mark the bug as forwarded to upstream, and
forward it to both the previous translator and their team (using the
corresponding debian-l10n-XXX mailing list).

.. _l10n-faqtr:

I18N & L10N FAQ for translators
================================================================================================================================

While reading this, please keep in mind that there is no general
procedure within Debian concerning these points, and that in any case,
you should collaborate with your team and the package maintainer.

.. _l10n-faqtr-help:

How to help the translation effort
--------------------------------------------------------------------------------------------------------------------------------

Choose what you want to translate, make sure that nobody is already
working on it (using your debian-l10n-XXX mailing list), translate it,
get it reviewed by other native speakers on your l10n mailing list, and
provide it to the maintainer of the package (see next point).

.. _l10n-faqtr-inc:

How to provide a translation for inclusion in a package
--------------------------------------------------------------------------------------------------------------------------------

Make sure your translation is correct (asking for review on your l10n
mailing list) before providing it for inclusion. It will save time for
everyone, and avoid the chaos resulting in having several versions of
the same document in bug reports.

The best solution is to file a regular bug containing the translation
against the package. Make sure to use both the ``patch`` and ``l10n``
tags, and to not use a severity higher than 'wishlist', since the lack
of translation never prevented a program from running.

.. _l10n-best:

Best current practice concerning l10n
================================================================================================================================

-  As a maintainer, never edit the translations in any way (even to
   reformat the layout) without asking on the corresponding l10n mailing
   list. You risk for example breaking the encoding of the file by doing
   so. Moreover, what you consider an error can be right (or even
   needed) in the given language.

-  As a translator, if you find an error in the original text, make sure
   to report it. Translators are often the most attentive readers of a
   given text, and if they don't report the errors they find, nobody
   will.

-  In any case, remember that the major issue with l10n is that it
   requires several people to cooperate, and that it is very easy to
   start a flamewar about small problems because of misunderstandings.
   So if you have problems with your interlocutor, ask for help on the
   corresponding l10n mailing list, on debian-i18n, or even on
   debian-devel (but beware, l10n discussions very often become
   flamewars on that list :)

-  In any case, cooperation can only be achieved with **mutual
   respect**.
